package de.drentech.innihald.system.documentrepository;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("dr")
public class DocumentRepositoryApplication extends Application {

}